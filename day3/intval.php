<?php
echo intval(42);
echo intval(4.2);
echo intval('42');
echo intval('+42');
echo intval('-42');
echo intval(042);
echo intval('042');
echo intval(1e10);
echo intval('1e10');
echo intval(0x1A);
echo intval(42000000);
echo intval(42000000000000000000000);
echo intval('4200000000000000000000');
echo intval(42,8);
echo intval('42',8);
echo intval(array());
echo intval(array('foo','bar'));